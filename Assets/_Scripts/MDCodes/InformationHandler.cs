﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InformationHandler : MonoBehaviour
{
    public static InformationHandler Instance;

    public GameObject panel_Information, btnInformation;
    public GameObject prfbMDCode, prfbLayerNumber;
    
    public Transform _Content;

    //public MDCodeInfoList _MDCodeInfoList = new MDCodeInfoList();
    public MDCodeInfoList currentMDCode = new MDCodeInfoList();

    MDCodeManager MDCodeManager;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        MDCodeManager = FindObjectOfType<MDCodeManager>();
    }

    public void UpdateMDCode_UI(int layerNo, string code, bool isOn)
    {
        if (MDCodeManager.isAllCode)
            return;

        FlashCard flashCard = MDCodeManager._MDCodeInfoList.layers[layerNo].codeList.Find(obj => obj.code.Equals(code));
        //Debug.Log(layerNo + " " + codeList + "  ");

        if (flashCard != null)
        {
            if (isOn)
            {
                currentMDCode.layers[layerNo].codeList.Add(flashCard);
            }
            else
            {
                currentMDCode.layers[layerNo].codeList.Remove(flashCard);
            }
        }


        if (isInformation)
        {
            FillMDCode();
        }

        //DisplyMDCode();
    }

    /*
    void DisplyMDCode()
    {
        txtMDCode.text = "";

        for (int i = 0; i < currentMDCode.layers.Count; i++)
        {
            for (int j = 0; j < currentMDCode.layers[i].codeList.Count; j++)
            {
                txtMDCode.text = txtMDCode.text + currentMDCode.layers[i].codeList[j].code + ",";
            }

            if (currentMDCode.layers[i].codeList.Count > 0)
                txtMDCode.text = txtMDCode.text + "\n";
        }

        txtMDCode.text = txtMDCode.text.Trim();
    }
    */

    bool isInformation;
    public void BtnInformation()
    {
        if (!isInformation)
        {
            isInformation = true;
            panel_Information.SetActive(true);

            MDCodeManager.ButtonState(true, false, btnInformation);

            FillMDCode();
        }
        else if (isInformation)
        {
            ResetInformation();
        }
    }

    public void ResetInformation()
    {
        foreach (var obj in _Content.GetComponentsInChildren<MDCode_Cell>())
        {
            DestroyImmediate(obj.gameObject);
        }

        isInformation = false;
        panel_Information.SetActive(false);

        MDCodeManager.ButtonState(false, false, btnInformation);
    }

    void FillMDCode()
    {
        foreach (var obj in _Content.GetComponentsInChildren<MDCode_Cell>())
        {
            DestroyImmediate(obj.gameObject);
        }

        for (int i = 0; i < currentMDCode.layers.Count; i++)
        {
            if (currentMDCode.layers[i].codeList.Count > 0)
            {
                GameObject objLayerNo = Instantiate(prfbLayerNumber, _Content, false);
                objLayerNo.name = "Layer " + (i + 1);
                objLayerNo.GetComponent<MDCode_Cell>().Init(currentMDCode.layers[i].layer);
            }

            for (int j = 0; j < currentMDCode.layers[i].codeList.Count; j++)
            {
                for (int k = 0; k < currentMDCode.layers[i].codeList[j].flashcards.Count; k++)
                {
                    if (_Content.Find("Layer_" + i + currentMDCode.layers[i].codeList[j].code) != null)
                        continue;

                    GameObject objCell = Instantiate(prfbMDCode, _Content, false);
                    objCell.name = "Layer_" + i + currentMDCode.layers[i].codeList[j].code;
                    objCell.GetComponent<MDCode_Cell>().Init(currentMDCode.layers[i].codeList[j].flashcards[k]);
                }
            }
        }
    }

    void FillMDCode(string code)
    {
        foreach (var obj in _Content.GetComponentsInChildren<MDCode_Cell>())
        {
            DestroyImmediate(obj.gameObject);
        }

        for (int i = 0; i < MDCodeManager._MDCodeInfoList.layers.Count; i++)
        {
            FlashCard flashCard = MDCodeManager._MDCodeInfoList.layers[i].codeList.Find(obj => obj.code.Equals(code));

            if (flashCard != null)
            {
                GameObject objLayerNo = Instantiate(prfbLayerNumber, _Content, false);
                objLayerNo.name = "Layer " + (i + 1);
                objLayerNo.GetComponent<MDCode_Cell>().Init(MDCodeManager._MDCodeInfoList.layers[i].layer);

                for (int j = 0; j < flashCard.flashcards.Count; j++)
                {
                    GameObject objCell = Instantiate(prfbMDCode, _Content, false);
                    objCell.name = flashCard.code;
                    objCell.GetComponent<MDCode_Cell>().Init(flashCard.flashcards[j]);
                }
            }
        }
    }

    public void MDCodeClicked()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 2500))
        {
            //Debug.Log("Hit tag ........" + hit.collider.name);

            if (isInformation)
                FillMDCode(hit.collider.name);
        }
    }

    public void CleareCurrenMDCode()
    {
        for (int i = 0; i < currentMDCode.layers.Count; i++)
        {
            currentMDCode.layers[i].codeList.Clear();
        }
    }
}
