﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DYNACodesHandler : MonoBehaviour
{
    public static DYNACodesHandler Instance;

    public GameObject buttonDYNACodes, buttonChemical, buttonMechanical;
    public GameObject panelDYNA, panelChemical, panelMechanical;
    MDCodeManager MDCodeManager;

    public List<BtnSign> btnsSigns = new List<BtnSign>();

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        MDCodeManager = FindObjectOfType<MDCodeManager>();
    }

 

    bool isDYNACodes;
    public void ButtonDYNACodes()
    {
        if (!isDYNACodes)
        {
            if (AttributesHandler.Instance.isAttributes)
                AttributesHandler.Instance.ResetAttributes();

            isDYNACodes = true;
            MDCodeManager.ButtonState(true, true, buttonDYNACodes);
            panelDYNA.SetActive(true);
        }
        else
        {
            ResetDYNA();
        }
    }

    public void ResetDYNA()
    {
        isDYNACodes = false;
        MDCodeManager.ButtonState(false, true, buttonDYNACodes);
        
        MDCodeManager.ButtonState(false, false, buttonChemical);
        MDCodeManager.ButtonState(false, false, buttonMechanical);

        panelDYNA.SetActive(false);
        panelChemical.SetActive(false);
        panelMechanical.SetActive(false);

        //MDCodeManager.ResetAllCode();

        int index = MDCodeManager._EmotionalAttributes.attributes.Count - 1;
        for (int i = 0; i < MDCodeManager._EmotionalAttributes.attributes[index].signs.Count; i++)
        {
           btnsSigns[i].DisableCode();
        }
    }

    public void BtnChecmical()
    {
        MDCodeManager.ButtonState(true, false, buttonChemical);
        MDCodeManager.ButtonState(false, false, buttonMechanical);

        panelChemical.SetActive(true);
        panelMechanical.SetActive(false);

        AssignSignButton(MDCodeManager._EmotionalAttributes.attributes.Count - 1);
    }

    public void AssignSignButton(int index)
    {
        for (int i = 0; i < btnsSigns.Count; i++)
        {
            btnsSigns[i].ResetButton();
            btnsSigns[i].gameObject.SetActive(false);
        }

        InformationHandler.Instance.CleareCurrenMDCode();

        for (int i = 0; i < MDCodeManager._EmotionalAttributes.attributes[index].signs.Count; i++)
        {
            btnsSigns[i].gameObject.SetActive(true);
            btnsSigns[i].Init(MDCodeManager._EmotionalAttributes.attributes[index].strAttributes, MDCodeManager._EmotionalAttributes.attributes[index].signs[i].strSign);
            btnsSigns[i].BtnClick();
        }
    }

    public void ButtonMechanical()
    {
        panelChemical.SetActive(false);
        panelMechanical.SetActive(true);

        MDCodeManager.ButtonState(false, false, buttonChemical);
        MDCodeManager.ButtonState(true, false, buttonMechanical);

        MDCodeManager.ResetAllCode();
    }
}
