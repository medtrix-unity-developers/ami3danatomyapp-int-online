﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttributesHandler : MonoBehaviour
{
    public static AttributesHandler Instance;

    public GameObject panel_Attributes;
    public List<BtnAttribute> attributesList = new List<BtnAttribute>();
    public Button buttonAttributes;
    MDCodeManager MDCodeManager;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        MDCodeManager = FindObjectOfType<MDCodeManager>();
    }

    public bool isAttributes = false;
    public void BtnEmotionalAttributes()
    {
        if (!isAttributes)
        {
            isAttributes = true;

            MDCodeManager.ResetAllCode();
            DYNACodesHandler.Instance.ResetDYNA();

            panel_Attributes.SetActive(true);

            InformationHandler.Instance.btnInformation.SetActive(true);

            MDCodeManager.ButtonState(true, true, buttonAttributes.gameObject);
        }
        else if (isAttributes)
        {
            InformationHandler.Instance.ResetInformation();
            ResetAttributes();
        }
    }

    public void BtnEmotionalAttributes(int index)
    {
        InformationHandler.Instance.btnInformation.SetActive(true);

        MDCodeManager.ResetAllCode();

        ResetAttributeButtons();

        attributesList[index].buttonImage.sprite = MDCodeManager.imgClicked;
        attributesList[index].buttonText.color = MDCodeManager.colClicked;
        
        InjectionTechniquesHandler.Instance.AssignSignButton(index);
    }

    public void ResetAttributes()
    {
        isAttributes = false;

        panel_Attributes.SetActive(false);
        InjectionTechniquesHandler.Instance.panel_Signs.SetActive(false);

        MDCodeManager.ButtonState(false, true, buttonAttributes.gameObject);

        ResetAttributeButtons();

        MDCodeManager.ResetAllCode();
    }

    void ResetAttributeButtons()
    {
        for (int i = 0; i < attributesList.Count; i++)
        {
            attributesList[i].buttonImage.sprite = MDCodeManager.imgNormal;
            attributesList[i].buttonText.color = MDCodeManager.colNormal;
        }
    }
}
