﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MDCodeManager : MonoBehaviour
{
    public static MDCodeManager Instance;

    public List<GameObject> objLayers = new List<GameObject>();
    public List<LayerHandler> mdCodeLayer = new List<LayerHandler>();

    public Slider slider;

    public Transform faceParentMD;
    public Transform _Camera;
    public GameObject _MD_RightFace;
    public VariableJoystick variableJoystick;
    public Transform _centerPoint;
    public bool isMD;

    public GameObject btnAllMDCode;

    public EmotionalAttributes _EmotionalAttributes = new EmotionalAttributes();
    public MDCodeInfoList _MDCodeInfoList = new MDCodeInfoList();
    
    public Sprite imgNormal, imgClicked;
    public Color colNormal, colClicked;

    public TextAsset textAsset;

    public Text txtMDCode;
    public bool isAllCode = false;

    public GameObject panelHelp;

    private void Awake()
    {
        Instance = this;
        panelHelp.SetActive(true);
        _EmotionalAttributes = JsonUtility.FromJson<EmotionalAttributes>(textAsset.text);
    }

    void Start()
    {
        Invoke("ResetModel", 1.0f);
    }

    void ResetModel()
    {
        for (int i = 0; i < objLayers.Count - 1; i++)
        {
            objLayers[i].transform.localPosition = Vector3.zero;
            objLayers[i].SetActive(false);
        }
    }

    public void OnSlider(Slider slider)
    {
        if (slider.value > 0)
        {
            if (slider.value <= 1.0f)
            {
                for (int i = 0; i < objLayers.Count - 1; i++)
                {
                    objLayers[i].SetActive(true);
                }

                if (isAllCode)
                {
                    mdCodeLayer[0].EnableDisableAllCode(false);

                    for (int i = 0; i < _EmotionalAttributes.attributes.Count - 1; i++)
                    {
                        Attributes _attribute = _EmotionalAttributes.attributes[i];
                        for (int j = 0; j < _attribute.signs.Count; j++)
                        {
                            mdCodeLayer[0].DisplayCode(_attribute.strAttributes, _attribute.signs[j].strSign, true, true);
                        }
                    }
                }
            }

            variableJoystick.gameObject.SetActive(true);
        }
        else
        {
            _centerPoint.transform.localPosition = new Vector3(-0.06f, 1.049f, 1.75f);
            variableJoystick.gameObject.SetActive(false);

            for (int i = 0; i < objLayers.Count - 1; i++)
            {
                objLayers[i].SetActive(false);
            }

            if(isAllCode)
                mdCodeLayer[0].EnableDisableAllCode(true);
        }

        for (int i = 0; i < objLayers.Count; i++)
        {
            objLayers[i].transform.localPosition = new Vector3(0, 0, slider.value * i * 1.0f);
        }
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && isAllCode)
        {
            InformationHandler.Instance.MDCodeClicked();
        }

        float zPos = _centerPoint.localPosition.z - variableJoystick.Horizontal * 0.3f;

        if (zPos < 1.75f || zPos > 40)
            return;

        _centerPoint.localPosition += new Vector3(0, 0, -variableJoystick.Horizontal * 0.3f);

    }

    public void BtnAllMDCode()
    {
        if (!isAllCode)
        {
            AttributesHandler.Instance.ResetAttributes();
            //DYNACodesHandler.Instance.ResetDYNA();

            isAllCode = true;
            btnAllMDCode.GetComponent<Image>().sprite = btnAllMDCode.GetComponent<Button>().spriteState.pressedSprite;
            
            InformationHandler.Instance.btnInformation.SetActive(true);

            for (int i = 0; i < _EmotionalAttributes.attributes.Count - 1; i++)
            {
                for (int j = 0; j < _EmotionalAttributes.attributes[i].signs.Count; j++)
                {
                    for (int k = 0; k < mdCodeLayer.Count; k++)
                    {
                        mdCodeLayer[k].DisplayCode(_EmotionalAttributes.attributes[i].strAttributes, _EmotionalAttributes.attributes[i].signs[j].strSign, true, true);
                    }
                }
            }

            if(slider.value == 0)
                mdCodeLayer[0].EnableDisableAllCode(true);
        }
        else
        {
            ResetAllCode();
        }
    }

    public void ResetAllCode()
    {
        isAllCode = false;
        btnAllMDCode.GetComponent<Image>().sprite = btnAllMDCode.GetComponent<Button>().spriteState.highlightedSprite;
        for (int i = 0; i < mdCodeLayer.Count; i++)
        {
            mdCodeLayer[i].EnableDisableAllCode(false);
        }
    }

    public void ButtonState(bool value, bool image, GameObject button)
    {
        if (image)
        {
            if (value)
                button.GetComponent<Image>().sprite = button.GetComponent<Button>().spriteState.pressedSprite;
            else
                button.GetComponent<Image>().sprite = button.GetComponent<Button>().spriteState.highlightedSprite;
        }
        else
        {
            if (value)
            {
                button.GetComponent<Image>().sprite = imgClicked;
                button.GetComponentInChildren<Text>().color = colClicked;
            }
            else
            {
                button.GetComponent<Image>().sprite = imgNormal;
                button.GetComponentInChildren<Text>().color = colNormal;
            }
        }
    }
}

[System.Serializable]
public class EmotionalAttributes
{
    public List<Attributes> attributes = new List<Attributes>();
}

[System.Serializable]
public class Attributes
{
    public string strAttributes;
    public List<Sign> signs = new List<Sign>();
}

[System.Serializable]
public class Sign
{
    public string strSign;
    //public string strCode;
    public List<string> codeList = new List<string>();
}

[System.Serializable]
public class MDCodeInfoList
{
    public List<Layer> layers = new List<Layer>();
}

/*
[System.Serializable]
public class MDCodeInfo
{
    public string code;
    public string product;
    public string facial_landmarks;
    public string target_layer;
    public string tool;
    public string delivery;
    public string volume_range;
}
*/

[System.Serializable]
public class Layer
{
    public string layer;
    public List<FlashCard> codeList = new List<FlashCard>();
}

[System.Serializable]
public class FlashCard
{
    public string code;
    public List<Sprite> flashcards;

    public FlashCard(string code)
    {
        this.code = code;
    }

    public FlashCard()
    {

    }
}