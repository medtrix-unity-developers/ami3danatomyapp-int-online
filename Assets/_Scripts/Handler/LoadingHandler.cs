﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingHandler : MonoBehaviour
{
    private void Awake()
    {
        //PlayerPrefs.DeleteAll();
    }

    void Start()
    {
        PlayerPrefs.SetString("CurrentLanguge", "EN");
        Invoke("LoadScene",0.1f);
	}

    void LoadScene()
    {
        if (PlayerPrefs.GetInt("IsLoggedIn") == 1)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Label");
        }
        else
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("Login");
        }
    }
}