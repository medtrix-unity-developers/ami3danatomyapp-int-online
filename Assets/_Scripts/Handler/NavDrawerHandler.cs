﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MaterialUI;

public class NavDrawerHandler : MonoBehaviour
{
    public static NavDrawerHandler Instance;


    public Animator animator;
    public GameObject panel_NavDrawer;
    public GameObject objText;

    private void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
	{

	}

	public void BtnAction(string action)
	{
        StartCoroutine(LoadScene(action));
	}

    IEnumerator LoadScene(string sceneName)
    {
        animator.SetTrigger("Open");
        MaterialNavDrawer.Instance.Close();

        //yield return new WaitForSeconds(0.5f);

        if (sceneName.Equals("MDCode"))
            objText.SetActive(true);
        
        yield return new WaitForSeconds(2.0f);
        SceneManager.LoadScene(sceneName);

        animator.SetTrigger("Close");

        yield return new WaitForSeconds(1);
        objText.SetActive(false);
    }

    public void Hide()
    {
        panel_NavDrawer.SetActive(false);
    }

    public void Show()
    {
        panel_NavDrawer.SetActive(true);
    }
}