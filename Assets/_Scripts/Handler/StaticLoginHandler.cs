﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaticLoginHandler : MonoBehaviour
{
    public Text txtError;
    public InputField inptUsername, inptPassword;
    public Text txtWelcome;
    public GameObject Panel_Login, PanelWelcome;

    private void Awake()
    {
    
    }

    void Start()
    {
        NavDrawerHandler.Instance.Hide();
    }

    public void BtnLogin()
    {
        if (inptUsername.text.ToLower().Equals("ami_essential") && inptPassword.text.Equals("3Danatomy"))
        {
            PlayerPrefs.SetInt("IsLoggedIn", 1);
            //GUIManager.Instance.ScreenController(ScreenName.Panel_Welcome);
            PanelWelcome.SetActive(true);
            Panel_Login.SetActive(false);
        }
        else
        {
            //txtError.text = "The username or password you have entered is incorrect. Please try again";
            txtError.gameObject.SetActive(true);
            Invoke("DisableError", 3);
        }
    }

    public void BtnWelcome()
    {     
        UnityEngine.SceneManagement.SceneManager.LoadScene("Label");
    }

    void DisableError()
    {
        txtError.gameObject.SetActive(false);//.text = "";
    }
}