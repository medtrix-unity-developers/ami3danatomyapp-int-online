﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LocalisationHandler : Singleton<LocalisationHandler>
{
    //public static LocalisationHandler Instance;

    [Serializable]
    public class LocalisationData
    {
        public List<LocalisationValue> data = new List<LocalisationValue>();
    }

    [Serializable]
    public class LocalisationValue
    {
        public string key;
        public string description;
    }

    public LocalisationData localisationData = new LocalisationData();

    public Font fontEN, fontCH, fontJP;

    new void Awake()
    {
        //Instance = this;
        var textAsset = Resources.Load<TextAsset>("Screen_" + PlayerPrefs.GetString("CurrentLanguge", "EN"));
        localisationData = JsonUtility.FromJson<LocalisationData>(textAsset.text);

        fontEN = Resources.Load("Font/Roboto-Medium") as Font;
        fontCH = Resources.Load("Font/FZZhongDengXian-Z07S") as Font;
        fontJP = Resources.Load("Font/Meiryo UI W53 Regular") as Font;
        //DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        
    }

    public string GetScreenText(string key)
    {
        return localisationData.data.Find(obj => obj.key.Equals(key)).description;
    }
}
