﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using UnityEngine.UI;
using System;

public class LabelsHandler : MonoBehaviour
{
    public static LabelsHandler Instance;


    [Serializable]
    public class LabelData
    {
        public List<LabelDesctiption> data = new List<LabelDesctiption>();
    }

    [Serializable]
    public class LabelDesctiption
    {
        public string key;
        public string title;
        public string description;
    }

    //private Dictionary<string, string> labelDec = new Dictionary<string, string>();

    public Text txtHeading, txtDescription;
    public LabelData labelData = new LabelData();

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        Debug.Log(PlayerPrefs.GetString("CurrentLanguge"));
        var textAsset = Resources.Load<TextAsset>("LabelDescription_"+PlayerPrefs.GetString("CurrentLanguge","EN"));
        labelData = JsonUtility.FromJson<LabelData>(textAsset.text);
    }

    //public string GetDescription(string key)
    //{
    //    return labelDec[key];
    //}

    public void ShowLabel(string key)
    {
        GUIManager.Instance.ScreenPopUp(ScreenName.Panel_LabelAdditional);
        //txtHeading.text = key.Replace("Label_", "").Replace("_"," ");
        txtHeading.text = GetTitle(key).Replace("\n", "");
        txtDescription.text = labelData.data.Find(obj => obj.key.Equals(key)).description;
    }

    public string GetTitle(string key)
    {
        return labelData.data.Find(obj => obj.key.Equals(key)).title;
    }

    public void BtnClose()
    {
        GUIManager.Instance.ScreenController(ScreenName.Panel_GamePlay);
    }
}