﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class DragHandeler : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
	public static GameObject itemBeingDragged;
	Vector3 startPosition;
	Transform startParent;
    public bool canDrag = true;
	#region IBeginDragHandler implementation

	public void OnBeginDrag (PointerEventData eventData)
	{
        if (!canDrag)
            return;

		itemBeingDragged = gameObject;
		startPosition = transform.position;
		startParent = transform.parent;
		GetComponent<CanvasGroup>().blocksRaycasts = false;
	}

	#endregion

	#region IDragHandler implementation

	public void OnDrag (PointerEventData eventData)
	{
        if (!canDrag)
            return;

        transform.position = eventData.position;
	}

    #endregion

    #region IEndDragHandler implementation

    public void OnEndDrag(PointerEventData eventData)
    {
        if (!canDrag)
            return;

        itemBeingDragged = null;
        GetComponent<CanvasGroup>().blocksRaycasts = true;

        if (transform.parent == startParent)
        {
            transform.position = startPosition;
        }
        else
        {
            canDrag = false;
            transform.localPosition = Vector3.zero;
        }

        QuizHandler.Instance.ActivateSubmitButton();
    }


    public void BtnUndo()
    {
        if (canDrag)
            return;

        transform.SetParent(startParent,false);
        canDrag = true;

        QuizHandler.Instance.ActivateSubmitButton();
    }

    #endregion
}
