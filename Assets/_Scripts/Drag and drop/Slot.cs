﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Slot : MonoBehaviour, IDropHandler
{
    public GameObject item
    {
        get
        {
            if (transform.childCount > 2)
            {
                return transform.GetChild(1).gameObject;
            }
            return null;
        }
    }

    #region IDropHandler implementation
    public void OnDrop(PointerEventData eventData)
    {
        //if (!DragHandeler.canDrag)
            //return;

        if (!item && DragHandeler.itemBeingDragged!= null)
        {
            DragHandeler.itemBeingDragged.transform.SetParent(transform);

            ExecuteEvents.ExecuteHierarchy<IHasChanged>(gameObject, null, (x, y) => x.HasChanged());
        }
    }
    #endregion
}