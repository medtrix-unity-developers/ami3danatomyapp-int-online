﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GUIManager : MonoBehaviour
{
    public static GUIManager Instance;
    public List<GameObject> objScreens = new List<GameObject>();
    private Dictionary<string, GameObject> dicScreens = new Dictionary<string, GameObject>();

    private Stack<ScreenName> screenHistory = new Stack<ScreenName>();
    ScreenName currentScreen = ScreenName.Panel_Login;

    public GameObject panel_Exit;
    public GameObject objFace, objFaceMD;

    public Color colDisable;
    public Text txtStatus;
    public List<Text> btnUndo = new List<Text>();
    public List<Text> btnTransparency = new List<Text>();
    public bool is3D = false;
   
    public Transform target3D, targetMD;

    private void Awake()
    {            
        Instance = this;

        for (int i = 0; i < objScreens.Count; i++)
        {
            dicScreens.Add(objScreens[i].name, objScreens[i]);
        }
    }

    void Start()
    {
        TglUndo(2);
        TglTransparency(2);

        is3D = true;
        NavDrawerHandler.Instance.Show();
    }

    public void TglUndo(int btnNo)
    {
        for (int i=0;i<btnUndo.Count;i++)
        {
            btnUndo[i].fontSize = 34;
            btnUndo[i].color = colDisable;
        }

        btnUndo[btnNo-1].fontSize = 50;
        btnUndo[btnNo-1].color = Color.white;
        AppManager.Instance._UndoMode = btnNo;
    }

    public void TglTransparency(int btnNo)
    {
        for (int i = 0; i < btnTransparency.Count; i++)
        {
            btnTransparency[i].fontSize = 34;
            btnTransparency[i].color = colDisable;
        }

        btnTransparency[btnNo - 1].fontSize = 50;
        btnTransparency[btnNo - 1].color = Color.white;
        AppManager.Instance._TransparencyMode = btnNo;
        AppManager.Instance.SetSlider();
    }

    public void BtnExit()
    {
        panel_Exit.SetActive(true);
    }

    public void BtnExit_Yes()
    {
        Application.Quit();
    }

    public void BtnExit_No()
    {
        panel_Exit.SetActive(false);
    }

    public void ScreenController(ScreenName str)
    {
        if (screenHistory.Contains(currentScreen))
            screenHistory.Pop();
        else
            screenHistory.Push(str);

        dicScreens["" + currentScreen].SetActive(false);
        dicScreens["" + str].SetActive(true);
        currentScreen = str;

        is3D = true;
    }

    public void ScreenPopUp(ScreenName str)
    {
        is3D = false;

        if (screenHistory.Contains(currentScreen))
            screenHistory.Pop();
        else
            screenHistory.Push(str);
            
        dicScreens["" + str].SetActive(true);
        currentScreen = str;
    }

    public ScreenName CurrentScreen
    {
        get
        {
            return screenHistory.Count == 0 ? ScreenName.Panel_Login : screenHistory.Peek();
        }
    }

    public void BtnReferance()
    {
        ScreenPopUp(ScreenName.Panel_Reference);
    }
}