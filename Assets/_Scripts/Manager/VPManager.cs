﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VPManager : MonoBehaviour
{
    public VideoClip[] videoClips;
    public VideoPlayer vp;

    public GameObject btnPlay, btnPause;

    private void Start()
    {
        btnPlay.SetActive(false);
        btnPause.SetActive(true);
        vp.prepareCompleted += OnPrepared;
    }

    private void OnDestroy()
    {
        vp.prepareCompleted -= OnPrepared;
    }

    public void BtnLoadVideo(int no)
    {
        vp.clip = videoClips[no];
        vp.Prepare();
        GUIManager.Instance.ScreenController(ScreenName.Panel_VideoPlayer);
    }

    public void BtnPlay()
    {
        vp.Play();
        btnPlay.SetActive(false);
        btnPause.SetActive(true);
    }

    public void BtnPause()
    {
        vp.Pause();
        btnPlay.SetActive(true);
        btnPause.SetActive(false);
    }

    public void BtnStop()
    {
        vp.Stop();
        btnPlay.SetActive(false);
        btnPause.SetActive(true);
        GUIManager.Instance.ScreenController(ScreenName.Panel_Video);
    }

    void OnPrepared(VideoPlayer vPlayer)
    {
        vp.Play();

    }
}
