﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellLabelDescription : MonoBehaviour
{
    public delegate void LabelInfoDelegate();
    public event LabelInfoDelegate OnLabelInfo;

    void Start()
    {
        //GetComponent<MeshRenderer>().material.renderQueue = 5000;//.sortingLayerName = "Default";
        //GetComponent<Renderer>().sortingOrder = 1;
    }

    private void OnMouseUp()
    {
        OnLabelInfo?.Invoke();
    }
}
