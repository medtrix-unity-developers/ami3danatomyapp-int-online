﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class QuizOptionCell : MonoBehaviour
{
    public static QuizOptionCell Instance;

    string sCorrectAnswer;
    public int correctIndex;
    public TextMeshProUGUI txtOption;
    public Text txtIndex;
    public Image imgStatus;
    QType qType;

    private void Awake()
    {
        Instance = this;
    }

    public void Init(int no,string sOption, string correctAns, int index, QType type)
    {
        if (type == QType.OPTION) {
            char c = (char)('A' + no);
            txtIndex.text = "" + c + ".";
        }

        qType = type;
        imgStatus.enabled = false;
        txtOption.text = sOption;
        sCorrectAnswer = correctAns;
        correctIndex = index;
    }

    public void BtnSelectAns()
    {
        QuizHandler.Instance.DisableOption();

        imgStatus.enabled = true;
        bool result = false;

        switch (qType)
        {
            case QType.OPTION:
                if (sCorrectAnswer.Equals(txtOption.text))
                    result = true;
                break;
            case QType.LOCATE:
                if (correctIndex == transform.parent.GetSiblingIndex())
                    result = true;
                break;
        }

        if (result)
        {
            imgStatus.color = Color.green;
            QuizHandler.Instance.UpdateScore();
        }
        else
        {
            imgStatus.color = Color.red;
        }

        StartCoroutine(Solve(QType.OPTION));
    }

    public bool CheckAnswerDrag()
    {
        imgStatus.enabled = true;
        bool result = false;

        QuizHandler.Instance.btnSubmit.interactable = false;

        if (!txtOption.text.Equals(GetComponentInParent<DropTarget>().strOption))
        {
            imgStatus.color = Color.red;
            result = false;
        }
        else
        {
            imgStatus.color = Color.green;
            result = true;
        }

        StartCoroutine(Solve(QType.DRAG_DROP));

        return result;
    }

    public void CheckAnswerOption()
    {
        bool result = false;

        switch (qType)
        {
            case QType.OPTION:
                if (sCorrectAnswer.Equals(txtOption.text))
                    result = true;
                break;
            case QType.LOCATE:
                if (correctIndex == transform.parent.GetSiblingIndex())
                    result = true;
                break;
        }

        if (result)
        {
            imgStatus.enabled = true;
            imgStatus.color = Color.green;
        }
    }

    IEnumerator Solve(QType type)
    {
        yield return new WaitForSeconds(1);

        if (type == QType.DRAG_DROP)
        {
            imgStatus.color = Color.green;
            txtOption.text = GetComponentInParent<DropTarget>().strOption;
        }
        else
        {
            QuizHandler.Instance.BtnSubmit();
        }
    }
}