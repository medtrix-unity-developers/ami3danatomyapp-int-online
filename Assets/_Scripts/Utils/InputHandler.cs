﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    public int mode;
    bool isPressed = false;
    float waitTime;

    //private void OnMouseDown()
    //{
    //    isPressed = true;
    //}

    //private void OnMouseUp()
    //{
    //    isPressed = false;
    //}

    private void OnMouseEnter()
    {
        if(AppManager.Instance.isPaintBrush)
            isPressed = true;
    }

    private void OnMouseExit()
    {
        isPressed = false;
    }

    void Update()
    {
        if (isPressed)
        {
            waitTime = +waitTime + Time.deltaTime;

            if(waitTime > 0.7f)
            {
                waitTime = 0;
                isPressed = false;
                AppManager.Instance.BtnDissect(mode);
            }
        }
    }
}
